**Description**

This folder contains the data files for the published work from our group.


"Paul et al 2016 Oncotarget" folder contains the Microarray Cel files used for "Paul JM, Toosi B, Vizeacoumar FS, Bhanumathy KK, Li Y,  Gerger C, El Zawily A, Freywald T, Anderson DH, Mousseau D, Kanthan R, Zhang Z, Vizeacoumar FJ*, Freywald A*. Targeting synthetic lethality between the SRC kinase and the EPHB6 receptor may benefit cancer treatment. Oncotarget. 2016 Aug 2;7(31):50027-50042." PMID: 27418135. 
